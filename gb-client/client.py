import urllib2
import urllib
import base64


url_to_post = 'http://tx-w10-vm04:8888/web_link/upload_file_content/'

with open("2.jpg", "rb") as image_file:
    encoded_string = base64.b64encode(image_file.read())

    data = dict()
    data['encoded_image'] = encoded_string
    url_values = urllib.urlencode(data)
    print url_values  # The order may differ.

    full_url = url_to_post + '?' + url_values
    data = urllib2.urlopen(full_url)
    print data.read()