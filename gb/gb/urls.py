from django.http import HttpResponse
from django.contrib import admin
from django.urls import include, path


def home_page(request):
    return HttpResponse("Ground Breakers Home Page")

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home_page),
    path('web_link/', include('web_link.urls')),
]
