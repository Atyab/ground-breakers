import hashlib
import base64
import json

from django.http import JsonResponse
from .models import ImageBucket


# CONSTANTS
FILE_PATH_PREFIX = './web_link/received_images/'
FILE_EXTENSION = '.jpg'


def upload_file_content(request):
    file_content = request.GET.get('encoded_image')
    context = dict()

    if file_content:
        file_name_as_id = hashlib.sha256(file_content.encode('utf-8')).hexdigest()

        existing_file_records = ImageBucket.objects.filter(file_name=file_name_as_id)
        if existing_file_records.count() == 0:
            image_instance = ImageBucket()
            image_instance.file_name = file_name_as_id
            image_instance.result = ""
            image_instance.is_processed = False
            image_instance.save()

            with open(''.join([FILE_PATH_PREFIX, image_instance.file_name, FILE_EXTENSION]), 'wb') as image_file:
                image_file.write(base64.b64decode(file_content))

            context['status'] = "SUCCESS"
            context['task_id'] = file_name_as_id
        else:
            context['status'] = "WARN"
            context['message'] = "Same file already exists. Get task status for task_id"
            context['task_id'] = file_name_as_id
    else:
        context['status'] = "WARN"
        context['message'] = "File content is empty."

    return JsonResponse(context)


def get_task(request):
    context = dict()
    unprocessed_files = ImageBucket.objects.filter(is_processed=False)
    if unprocessed_files.count() > 0:
        file_to_return = unprocessed_files[0]

        with open(''.join([FILE_PATH_PREFIX, file_to_return.file_name, FILE_EXTENSION]), 'rb') as image_file:
            encoded_file_content = base64.b64encode(image_file.read())
            d = encoded_file_content.decode('utf-8')
            context['encoded_file_content'] = json.dumps(d)
            context['file_name_as_id'] = file_to_return.file_name
            context['status'] = "SUCCESS"
    else:
        context['status'] = "EMPTY"

    return JsonResponse(context)


def mark_task_done(request):
    task_id = request.GET.get('task_id')
    result = request.GET.get('result')
    context = dict()

    if task_id and result:
        number_of_records = ImageBucket.objects.filter(file_name=task_id, is_processed=False).count()
        if number_of_records > 0:
            file_record = ImageBucket.objects.filter(file_name=task_id, is_processed=False)[0]
            file_record.result = result
            file_record.is_processed = True
            file_record.save()
            context['status'] = "SUCCESS"
            context['message'] = "Marked done successfully."
        else:
            context['status'] = "WARN"
            context['message'] = "Task ID may be in valid"
    else:
        context['status'] = "WARN"
        context['message'] = "Get parameters task_id OR result not provided."
    return JsonResponse(context)


def get_task_status(request):
    task_id = request.GET.get("task_id")
    context = dict()

    if task_id:
        number_of_records = ImageBucket.objects.filter(file_name=task_id).count()

        if number_of_records > 0:
            file_record = ImageBucket.objects.get(file_name=task_id)

            if file_record.result:
                context['result'] = file_record.result
                context['status'] = "DONE"
            else:
                context['status'] = "IN PROGRESS"
        else:
            context['status'] = "WARN"
            context['message'] = "Task ID may be invalid"
    else:
        context['status'] = "WARN"
        context['message'] = "Get parameters task_id not provided."

    return JsonResponse(context)


def reset_db(request):
    ImageBucket.objects.all().delete()
    return JsonResponse({'status': "SUCCESS", 'message': 'db records are deleted'})
