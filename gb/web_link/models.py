from django.db import models


class ImageBucket(models.Model):
    file_name = models.CharField(max_length=200)
    result = models.CharField(max_length=100)
    is_processed = models.BooleanField(default=False)
