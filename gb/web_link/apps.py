from django.apps import AppConfig


class WebLinkConfig(AppConfig):
    name = 'web_link'
