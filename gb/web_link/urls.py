from django.urls import path

from . import views


urlpatterns = [
    path(r'upload_file_content/', views.upload_file_content, name='upload_file_content'),
    path(r'get_task/', views.get_task, name='get_task'),
    path(r'mark_task_done/', views.mark_task_done, name='mark_task_done'),
    path(r'get_task_status/', views.get_task_status, name='get_task_status'),
    path(r'reset_db/', views.reset_db, name='reset_db'),
]
